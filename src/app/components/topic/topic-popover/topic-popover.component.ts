import {Component, OnInit} from '@angular/core';
import {PopoverController} from '@ionic/angular';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {DatePipe} from '@angular/common';
import {UtilService} from '../../../@core/services/util.service';

@Component({
    selector: 'app-topic-popover',
    templateUrl: './topic-popover.component.html',
    styleUrls: ['./topic-popover.component.scss'],
})
export class TopicPopoverComponent implements OnInit {

    maxAttempts = 0;
    group: any;

    constructor(
        private router: Router,
        private popover: PopoverController,
        public datepipe: DatePipe,
        private utilService: UtilService,
    ) {
    }

    ngOnInit() {
        this.maxAttempts = this.utilService.getParsedJwt().value ? this.utilService.getParsedJwt().value : environment.env.max_attempt;
    }

    goTo(pensumAssigned) {
        this.dismissClick();
        this.router.navigate([`topic-resume/${pensumAssigned.tema_id}`]);
    }

    async dismissClick() {
        await this.popover.dismiss();
    }

    hasValidDate(deadLine) {
        const deadLineTimesTamp = (new Date(deadLine)).getTime() / 1000;
        const currentTimesTamp = (new Date()).getTime() / 1000;
        return currentTimesTamp >= deadLineTimesTamp ? true : false;
    }

}
